export default {
  color: {
    primary: {
      normal: '#2961FF',
    },
    text: {
      normal: '#2D3240',
      light: '#676D80',
    },
    warning: '#e02424',
  },
  font: {
    size: {
      tiny: '12px',
      small: '14px',
      normal: '16px',
      title: {
        normal: '20px',
        large: '28px',
      },
    },
    lineHeight: {
      tiny: '20px',
      small: '22px',
      normal: '24px',
      title: {
        normal: '28px',
        large: '36px',
      },
    },
    weight: {
      normal: '400',
      semibold: '600',
      bold: '700',
    },
  },
}
